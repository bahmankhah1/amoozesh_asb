<?php
/*
insert tables
wp_user_tokens
wp_confirm_codes
wp_user_credit
wp_user_files

*/
define('SHORTINIT',1);
define( 'WP_DEBUG', true );
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-settings.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-includes/category.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-includes/post.php';

define('WC_ABSPATH',$_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/woocommerce/');
global $wpdb;

// load api files
require_once('loader.php');

// $api = new coreclass();
$uri = get_request_uri();
$uri = str_replace(base_url() . '/', '/', $uri);

$parts = explode('/', $uri);
$parts = array_slice($parts,2);
global $version;
$version = $parts[0];
$controller = $parts[1];
$method = $parts[2];
spl_autoload_register('require_class');
// main api class
new api();

$params = array();
for ($i=3; $i<count($parts); $i++){
  $params[] = $parts[$i];
}

$controllerInstance = new $controller();
call_user_func_array(array($controllerInstance, $method), $params);
