<?php

class HomeModel
{
    static public function getHome()
    {
        global $wpdb;
        global $prefix;
        $results = $wpdb->get_results("SELECT * FROM {$prefix}posts WHERE post_type = 'product' AND post_status = 'publish' AND ID NOT IN (SELECT meta_value FROM {$prefix}postmeta WHERE meta_key = '_tutor_course_product_id') ORDER BY post_date DESC LIMIT 5");
        $products = [];
        foreach ($results as $product) {
            $meta = get_meta('postmeta', $product->ID);

            $products[] = array(
                'id' => $product->ID,
                'title' => $product->post_title,
                'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
                'price' => get_from_array($meta, '_price'),
                'regularPrice' => get_from_array($meta, '_regular_price'),
                'salePrice' => get_from_array($meta, '_sale_price'),
            );
        }

        $results = $wpdb->get_results("SELECT `ID` FROM {$prefix}posts WHERE post_type = 'courses' AND post_status = 'publish' ORDER BY post_date DESC LIMIT 10");

        $courses = [];
        $freeVideos = [];

        foreach ($results as $course) {
            $meta = get_meta('postmeta', $course->ID);
            if (get_from_array($meta, '_tutor_course_price_type') == 'paid') {
                $courses[] = CourseModel::get($course->ID);
            } else {
                /* $freeVideos[] = array(
                    'id' => $course->ID,
                    'title' => $course->post_title,
                    'content' => $course->post_content,
                    'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
                    'courseVideoUrl' => $srcVideo,
                ); */
                $freeVideos[] = CourseModel::get($course->ID, true);
            }
        }

        $results = $wpdb->get_results("SELECT * FROM {$prefix}posts WHERE post_type = 'post' AND post_status = 'publish' ORDER BY post_date DESC LIMIT 5");

        $posts = [];
        foreach ($results as $post) {
            $meta = get_meta('postmeta', $post->ID);

            $posts[] = array(
                'id' => $post->ID,
                'title' => $post->post_title,
                'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
                'content' => $post->post_content,
            );
        }


        return ['freeVideos' => $freeVideos, 'courses' => $courses, 'products' => $products,  'posts' => $posts];
    }
}
