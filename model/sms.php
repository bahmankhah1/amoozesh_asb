<?php

class smsModel
{
	public static function send_sms($to, $args = null)
	{

		global $config;

		$url = "https://api2.ippanel.com/api/v1/sms/pattern/normal/send";

		$param = [
			"code" => $config['sms']['pattern'],
			"sender" => "+983000505",
			"recipient" => $to,
			"variable" => $args,
		];

		$handler = curl_init($url);
		curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($handler, CURLOPT_POSTFIELDS, json_encode($param));
		curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handler, CURLOPT_HTTPHEADER, ["apikey: {$config['sms']['accessKey']}", 'accept: */*', 'Content-Type: application/json']);
		$response2 = curl_exec($handler);

		$response2 = json_decode($response2, true);

		return $response2;
	}
	public static function send_sms_pattern($phone, $pattern, $args = null)
	{
		if (!$args) {
			return false;
		}
		global $config;

		$to      = array($phone);
		$number  = $config['sms']['number'];
		$url     = "https://ippanel.com/patterns/pattern?username=" . $config['sms']['user'] . "&password=" . urlencode($config['sms']['pass']) . "&from=$number&to=" . json_encode($to) . "&input_data=" . urlencode(json_encode($args)) . "&pattern_code=$pattern";
		$handler = curl_init($url);
		curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($handler, CURLOPT_POSTFIELDS, $args);
		curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($handler);

		return $response;
	}

	public static function send_login_code($phone)
	{
		global $wpdb;
		global $prefix;
		$check_code = $wpdb->get_row("SELECT * FROM " . $prefix . "confirm_codes WHERE value LIKE '%$phone' AND status = 0");
		$code       = strval(rand(100000, 999999));

		$response = self::send_sms($phone, ['codec' => $code]);
		//$response = array('code' => 200);
		if ($response && $response['code'] == 200) {

			if (!$check_code) {
				
				$res = $wpdb->insert($prefix . 'confirm_codes', array(
					'type'       => 'sms',
					'value'      => $phone,
					'code'       => $code,
					'status'     => 0,
					'ip'         => get_user_ip(),
					'sms_status' => 1
				));
			} else {
				$res = $wpdb->update($prefix . 'confirm_codes', array(
					'code'         => $code,
					'sms_status' => 1
				), array(
					'id' => $check_code->id
				));
			}
		} else {
			return null;
		}

		return intval($code);
	}

	public static function verify_login_code($input, $code)
	{
		global $wpdb;
		global $prefix;
		$sql   = "SELECT * FROM " . $prefix . "confirm_codes WHERE `value` LIKE '%$input' AND code = '$code' AND status = '0'";
		$check = $wpdb->get_row($sql);
		if ($check) {
			$wpdb->update($prefix . 'confirm_codes', array(
				'status' => 1
			), array(
				'id' => $check->id
			));

			return true;
		}

		return null;
	}
}
