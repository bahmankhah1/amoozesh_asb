<?php
class CourseModel
{
	public static function myCourses($user_id)
	{
		global $wpdb;
		global $prefix;
		$sql = "SELECT post_id FROM {$prefix}user_files WHERE user_id = $user_id ";
		$rows = $wpdb->get_results($sql);
		$courses = [];

		foreach ($rows as $r) {
			$courses[] = self::get($r->post_id);
		}

		return $courses;
	}

	public static function getList($page_index = 1, $page_size = 10, $order = 'DESC')
	{

		global $wpdb;
		global $prefix;
		$offset = ($page_index - 1) * $page_size;
		$sql    = "SELECT * FROM {$prefix}posts WHERE `post_type` = 'courses' AND `post_status` = 'publish' ORDER BY `post_date` $order LIMIT $offset,$page_size";

		$results = $wpdb->get_results($sql);
		$list       = array();
		foreach ($results as $result) {

			$meta = get_meta('postmeta', $result->ID);
			if (get_from_array($meta, '_tutor_course_price_type') == 'paid') {
				$list[] = self::get($result->ID);
			}
		}

		return $list;
	}

	public static function getAuthorName($id)
	{
		global $wpdb;
		global $prefix;
		$course = $wpdb->get_row("SELECT `display_name` FROM {$prefix}users u INNER JOIN {$prefix}posts p ON p.`post_author` = u.`ID` WHERE p.`ID` = '$id' AND p.`post_status` = 'publish'");

		if ($course) {
			return $course->display_name;
		}
		return '';
	}

	public static function get($id, $free = false)
	{
		global $wpdb;
		global $prefix;
		$course = $wpdb->get_row("SELECT * FROM " . $prefix . 'posts' . " WHERE `ID` = '$id' AND `post_status` = 'publish' AND `post_type` = 'courses' ");
		if (!$course) {
			return null;
		}

		$meta = get_meta('postmeta', $id);
		$courseVideoArray = maybe_unserialize(get_from_array($meta, '_video'));
		$courseVideo = $wpdb->get_row("SELECT guid from {$prefix}posts WHERE ID = " . $courseVideoArray['source_video_id'] . " ");
		$duration = maybe_unserialize(get_from_array($meta, '_course_duration'));

		$topics = $wpdb->get_results("SELECT `id`, `post_title` AS `title` FROM `{$prefix}posts` WHERE `post_parent` = $id AND `post_type` = 'topics' ", ARRAY_A);
		foreach ($topics as &$val) {
			$lessons = $wpdb->get_results("SELECT `id`, `post_title` AS `title` FROM `{$prefix}posts` WHERE `post_parent` = {$val['id']} AND `post_type` = 'lesson' ", ARRAY_A);
			foreach ($lessons as &$l) {
				$lMeta = get_meta('postmeta', $l['id']);
				$videoMeta = maybe_unserialize($lMeta['_video']);
				$videoPost = $wpdb->get_row("SELECT * FROM {$prefix}posts WHERE `ID` = {$videoMeta['source_video_id']}");
				$l['video'] = $videoPost->guid;
				$l['runtime'] = $videoMeta['runtime'];
			}
			$val['lessons'] = $lessons;
		}

		if ($free) {
			return array(
				'id' => $course->ID,
				'title' => $course->post_title,
				'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
				'postDate' => $course->post_date,
				'postContent' => $course->post_content,
				'topics' => $topics,
				'courseVideoUrl' => $courseVideo->guid,
				'duration' => $duration,
				'author' => self::getAuthorName($course->ID),
			);
		}

		$productMeta = get_meta('postmeta', get_from_array($meta, '_tutor_course_product_id'));

		return array(
			'id' => $course->ID,
			'title' => $course->post_title,
			'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
			'price' => get_from_array($productMeta, '_price'),
			'regularPrice' => get_from_array($productMeta, '_regular_price'),
			'salePrice' => get_from_array($productMeta, '_sale_price'),
			'postDate' => $course->post_date,
			'postContent' => $course->post_content,
			'topics' => $topics,
			'courseVideoUrl' => $courseVideo->guid,
			'duration' => $duration,
			'author' => self::getAuthorName($course->ID),
			//			'breadcrumbs'=>get_post_breadcrumbs($course->ID),
		);
	}
	public static function getCompleteCourse($id)
	{
		global $wpdb;
		global $prefix;
		$course = $wpdb->get_row("SELECT * FROM " . $prefix . 'posts' . " WHERE `ID` = $id AND `post_status` = 'publish' AND `post_type` = 'courses' ");
		if (!$course) {
			return null;
		}
		$topics = $wpdb->get_results("SELECT * FROM {$prefix}posts WHERE `post_parent` = {$course->ID} AND `post_type` = 'topics' ");
		$topicsArr = [];
		foreach ($topics as $t) {

			$topic = array();
			$topic['title'] = $t->post_title;
			$lessons = $wpdb->get_results("SELECT * FROM {$prefix}posts WHERE `post_parent` = {$t->ID} AND `post_type` = 'lesson' ");
			$topic['lessons'] = [];
			foreach ($lessons as $l) {
				$lessonArr = [];
				$meta = get_meta('postmeta', $l->ID);
				$video = maybe_unserialize(get_from_array($meta, '_video'));
				$videoPost = $wpdb->get_row("SELECT * FROM {$prefix}posts WHERE `ID` = {$video['source_video_id']}");

				$lessonArr['title'] = $l->post_title;
				$lessonArr['video'] = $videoPost->guid;
				$lessonArr['content'] = $l->post_content;
				array_push($topic['lessons'], $lessonArr);
			}
			array_push($topicsArr, $topic);
			//$topicsArr[] = $topic;
		}

		return array(
			'id' => $course->ID,
			'topics' => $topicsArr,
			'title' => $course->post_title,
			'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
			'postDate' => $course->post_date,
			'postContent' => $course->post_content,
			'topics' => $topics,
			'author' => self::getAuthorName($course->ID),
			//			'breadcrumbs'=>get_post_breadcrumbs($course->ID),
		);
	}
}
