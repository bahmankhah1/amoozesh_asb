<?php

class UserModel {

	public static function create_new_user( $username, $password ) {
		global $wpdb;
		global $prefix;

		$result = $wpdb->insert( $prefix . 'users', array(
			'user_login'          => $username,
			'user_pass'           => md5( $password ),
			'user_nicename'       => $username,
			'user_email'          => '',
			'user_url'            => '',
			'user_registered'     => date( 'Y-m-d H:i:s' ),
			'user_activation_key' => '',
			'user_status'         => 0,
			'display_name'        => $username
		) );

		$user_id = $wpdb->insert_id;

		if ( $wpdb->insert_id ) {
			$wpdb->insert( $prefix . 'user_credit', array(
				'user_id'  => $user_id,
				'credit'   => 0,
				'currency' => 'TOMAN',
			) );

			$wpdb->insert( $prefix . 'usermeta', array(
				'user_id'    => $user_id,
				'meta_key'   => $prefix . 'capabilities',
				'meta_value' => maybe_serialize( array( 'subscriber' => 1 ) )
			) );

			$wpdb->insert( $prefix . "usermeta", array(
				'user_id'    => $user_id,
				'meta_key'   => $prefix . 'user_level',
				'meta_value' => 0
			) );
		}

		if ( $result ) {
			return $user_id;
		}

		return null;
	}

	public static function updateFcmToken(){
		global $wpdb;
		global $prefix;
		global $user_token;
		global $json;
		$res = $wpdb->update($prefix.'user_tokens', array('deviceToken'=>$json->fcmToken), array('token'=>$user_token));
		return $res;
	}

	public static function validate() {
		global $wpdb;
		global $prefix;
		global $user_token;
		$check = $wpdb->get_row( "SELECT * FROM " . $prefix . "user_tokens t INNER JOIN " . $prefix . "users u on u.id = t.user_id WHERE t.token LIKE '$user_token' AND t.token_status = 1" );
		if ( $check ) {
			return $check->user_id;
		}

		return false;
	}

	public static function loginUser( $input ) {
		global $json;
		global $wpdb;
		global $prefix;

		if ( filter_var( $input, FILTER_VALIDATE_EMAIL ) ) {
			$input = sanitize_text_field( $input );
		} else {
			$input = $input ;
		}

		$code           = strval($json->code);
		$firebase_token = $json->firebaseToken;
		$token          = generate_password( 25, false );

		$response = smsModel::verify_login_code( $input, $code );
		if ( $response ) {
			$user = $wpdb->get_row( "SELECT * FROM " . $prefix . "usermeta WHERE meta_key = 'digits_phone_no' AND meta_value LIKE '%$input'" );
			if ( ! $user ) {
				$type     = 'register';
				$username = get_radnom_unique_username();
				$user_id  = self::create_new_user( $username, generate_password() );
				$wpdb->insert( $prefix . 'usermeta', array(
					'meta_key'   => 'digits_phone_no',
					'meta_value' => $input,
					'user_id'    => $user_id
				) );

				$wpdb->insert( $prefix . 'usermeta', array(
					'meta_key'   => 'digits_phone',
					'meta_value' => '+98' . $input,
					'user_id'    => $user_id
				) );

				$wpdb->insert( $prefix . "usermeta", array(
					'meta_key'   => 'nickname',
					'meta_value' => $username,
					'user_id'    => $user_id
				) );

				$wpdb->insert( $prefix . "usermeta", array(
					'meta_key'   => 'first_name',
					'meta_value' => $username,
					'user_id'    => $user_id
				) );

				$user_obj = self::reformat_user_object( $user_id );

			} else {
				$type     = 'login';
				$user_obj = self::reformat_user_object( $user->user_id );
				$user_id  = $user->user_id;

				$check_device = $wpdb->get_row( "SELECT * FROM $prefix" . "user_tokens WHERE `user_id` = '$user_id'" );
			}
 
			$wpdb->insert( $prefix . "user_tokens", array(
				'token'        => $token,
				'user_id'      => $user_id,
				'create_date'  => date( 'Y-m-d H:i:s' ),
				'device_token' => $firebase_token??'',
				'token_status' => 1
			) );

			return array(
				'user_object' => $user_obj,
				'token'       => $token,
				'type'        => $type
			);
		} else {
			return null;
		}
	}

	public static function loginUserWeb( $input, $code, $firebaseToken ) {
		global $wpdb;
		global $prefix;

		if ( filter_var( $input, FILTER_VALIDATE_EMAIL ) ) {
			$input = sanitize_text_field( $input );
		} else {
			$input = intval( $input );
		}

		$token = generate_password( 25, false );

		$response = smsModel::verify_login_code( $input, $code );
		if ( $response ) {
			$user = $wpdb->get_row( "SELECT * FROM " . $prefix . "usermeta WHERE meta_key = 'digits_phone_no' AND meta_value LIKE '%$input'" );
			if ( ! $user ) {
				$type     = 'register';
				$username = get_radnom_unique_username();
				$user_id  = self::create_new_user( $username, generate_password() );
				$wpdb->insert( $prefix . 'usermeta', array(
					'meta_key'   => 'digits_phone_no',
					'meta_value' => $input,
					'user_id'    => $user_id
				) );

				$wpdb->insert( $prefix . 'usermeta', array(
					'meta_key'   => 'digits_phone',
					'meta_value' => '+98' . $input,
					'user_id'    => $user_id
				) );

				$wpdb->insert( $prefix . "usermeta", array(
					'meta_key'   => 'nickname',
					'meta_value' => $username,
					'user_id'    => $user_id
				) );

				$wpdb->insert( $prefix . "usermeta", array(
					'meta_key'   => 'first_name',
					'meta_value' => $username,
					'user_id'    => $user_id
				) );

				$user_obj = self::reformat_user_object( $user_id );

			} else {
				$type     = 'login';
				$user_obj = self::reformat_user_object( $user->user_id );
				$user_id  = $user->user_id;

				$check_device = $wpdb->get_row( "SELECT * FROM $prefix" . "user_tokens WHERE `user_id` = '$user_id'" );
			}

			$wpdb->insert( $prefix . "user_tokens", array(
				'token'        => $token,
				'user_id'      => $user_id,
				'create_date'  => date( 'Y-m-d H:i:s' ),
				'device_token' => $firebaseToken,
				'token_status' => 1
			) );

			return $user_id;
		} else {
			return false;
		}
	}

	public static function loginUser2( $input ) {
		global $json;
		global $wpdb;
		global $prefix;

		if ( filter_var( $input, FILTER_VALIDATE_EMAIL ) ) {
			$input = sanitize_text_field( $input );
		} else {
			$input = intval( $input );
		}

		$code           = $json->code;
		$firebase_token = $json->firebaseToken;
		$token          = generate_password( 25, false );
		$device_id      = ( isset( $json->DeviceID ) ) ? $json->DeviceID : null;
		$device_model   = ( isset( $json->DeviceModel ) ) ? $json->DeviceModel : null;

		$response = smsModel::verify_login_code( $input, $code );
		if ( $response ) {
			$user = $wpdb->get_row( "SELECT * FROM " . $prefix . "usermeta WHERE meta_key = 'digits_phone_no' AND meta_value LIKE '%$input'" );
			if ( ! $user ) {
				$type     = 'register';
				$username = get_radnom_unique_username();
				$user_id  = self::create_new_user( $username, generate_password() );
				$wpdb->insert( $prefix . 'usermeta', array(
					'meta_key'   => 'digits_phone_no',
					'meta_value' => $input,
					'user_id'    => $user_id
				) );

				$wpdb->insert( $prefix . 'usermeta', array(
					'meta_key'   => 'digits_phone',
					'meta_value' => '+98' . $input,
					'user_id'    => $user_id
				) );

				$wpdb->insert( $prefix . "usermeta", array(
					'meta_key'   => 'nickname',
					'meta_value' => $username,
					'user_id'    => $user_id
				) );

				$wpdb->insert( $prefix . "usermeta", array(
					'meta_key'   => 'first_name',
					'meta_value' => $username,
					'user_id'    => $user_id
				) );

				$user_obj = self::reformat_user_object( $user_id );

			} else {
				$type     = 'login';
				$user_obj = self::reformat_user_object( $user->user_id );
				$user_id  = $user->user_id;

				$check_device = $wpdb->get_row( "SELECT * FROM $prefix" . "user_tokens WHERE `user_id` = '$user_id'" );
				if ( $check_device ) {
					if ( strcmp( $check_device->device_id, $device_id ) === 0 ) {
						$wpdb->update( $prefix . "user_tokens", array(
							'token_status' => 0
						), array(
							'user_id' => $user_id
						) );
					} else {
						return array(
							'device_model' => $check_device->device_model,
							'status'       => false
						);
					}
				}
			}

			$wpdb->insert( $prefix . "user_tokens", array(
				'token'        => $token,
				'user_id'      => $user_id,
				'create_date'  => date( 'Y-m-d H:i:s' ),
				'device_token' => $firebase_token,
				'device_id'    => $device_id,
				'device_model' => $device_model,
				'token_status' => 1
			) );

			return array(
				'status'      => true,
				'user_object' => $user_obj,
				'token'       => $token,
				'type'        => $type
			);
		} else {
			return null;
		}
	}

	public static function reformat_user_object( $user_id ) {
		global $wpdb;
		global $prefix;

		$new_obj = self::profile_object( $user_id );

		return $new_obj;
	}

	public static function logoutUser() {
		global $user_token;
		global $wpdb;
		global $prefix;

		if ( ! $user_token ) {
			return false;
		}
//		$res = $wpdb->delete( $prefix . "user_tokens", array(
//			'token' => $user_token
//		) );

		$res = $wpdb->update( $prefix . "user_tokens", array(
			'token_status' => 0
		), array(
			'token' => $user_token
		) );

		return $res;
	}

	public static function user_object_tiny( $user_id, $role = null ) {
		global $wpdb;
		global $prefix;
		global $config;

		if ( intval( $user_id ) === 0 ) {
			return [
				'id'          => 0,
				'fullName'    => 'کاربر',
				'imageAvatar' => $config['home_url'] . '/user-profile.jpg'
			];
		}

		$user_object = $wpdb->get_row( "SELECT * FROM " . $prefix . "users WHERE " . $prefix . "users.ID = '$user_id'" );
		$usermetas   = get_user_meta( $user_id );
		if ( ! $user_object ) {
			return [
				'id'          => 0,
				'fullName'    => 'کاربر',
				'imageAvatar' => $config['home_url'] . '/user-profile.jpg'
			];
		}
		$imgAvatar = ( get_from_array( $usermetas, '_user_profile' ) ) ? get_from_array( $usermetas, '_user_profile' ) : $config['home_url'] . '/user-profile.jpg';
		if ( is_numeric( $imgAvatar ) ) {
			$imgAvatar = get_post_thumbnail_url( $imgAvatar );
		}
		$new_obj = array(
			'id'          => intval( $user_id ),
			'fullName'    => $user_object->display_name,
			'imageAvatar' => $imgAvatar
		);

		if ( $role ) {
			$new_obj['role'] = $role;
		}

		return $new_obj;
	}

	public static function profile_object( $user_id ) {
		$user_id = intval( $user_id );
		global $wpdb;
		global $prefix;
		global $config;
		if ( $user_id ) {
			$data        = $wpdb->get_row( "SELECT * FROM $prefix" . "users WHERE `ID` = $user_id" );
			$meta        = get_meta( 'usermeta', $user_id );
			$imageAvatar = ( get_from_array( $meta, '_user_profile' ) ) ? get_from_array( $meta, '_user_profile' ) : $config['home_url'] . '/user-profile.jpg';
			$credit      = $wpdb->get_row( "SELECT `credit` FROM $prefix" . "user_credit WHERE `user_id` = $user_id" );
			$obj         = [
				'id'          => $user_id,
				'fullName'    => $data->display_name,
				'wallet'      => ( $credit ) ? strval( $credit->credit ) : "0",
				'imageAvatar' => $imageAvatar,
				'phoneNumber' => get_from_array( $meta, 'digits_phone_no' )
			];

			return $obj;
		}

		return null;
	}

	public static function upload_profile_picture( $file, $user_id ) {
		global $config;
		global $wpdb;
		global $prefix;

		$upload_dir = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/profile/';
		$newname    = "profile-" . date( 'YmdHis' ) . rand( 1000, 9999 ) . ".jpg";
		$path       = date( 'Y' ) . "/" . date( 'm' ) . "/" . date( 'd' );
		$dist       = $upload_dir . $path . '/';

		$url   = $config['home_url'] . '/wp-content/uploads/profile/' . $path . '/' . $newname;
		$mkdir = makedir( $dist );
		if ( $mkdir ) {
			$move = move_uploaded_file( $file['tmp_name'], $dist . $newname );
			if ( $move ) {
				$check = $wpdb->get_row( "SELECT * FROM $prefix" . "usermeta WHERE user_id = $user_id AND meta_key = '_user_profile'" );
				if ( $check ) {
					$wpdb->update( $prefix . 'usermeta', array(
						'meta_value' => $url,
					), array(
						'meta_key' => '_user_profile',
						'user_id'  => $user_id
					) );
				} else {
					$wpdb->insert( $prefix . 'usermeta', array(
						'user_id'    => $user_id,
						'meta_key'   => '_user_profile',
						'meta_value' => $url
					) );
				}

				return $url;
			}
		}

		return null;
	}

	public static function get_author_contents( $user_id ) {
		global $wpdb;
		global $prefix;
		$user_id   = intval( $user_id );
		$sql       = "SELECT * FROM $prefix" . "posts posts LEFT OUTER JOIN $prefix" . "postmeta postmeta ON posts.ID = postmeta.post_id WHERE postmeta.meta_key = '_content_author' AND postmeta.meta_value = $user_id AND posts.post_status LIKE 'publish' AND (posts.post_type LIKE 'audiobook' OR posts.post_type LIKE 'ebook' OR posts.post_type LIKE 'podcast')";
		$user_data = $wpdb->get_results( $sql );

		$audiobooks = [];
		$ebooks     = [];

		if ( $user_data ) {
			foreach ( $user_data as $item ) {
				if ( $item->post_type == 'audiobook' ) {
					$audiobooks[] = audiobookModel::get( $item->ID, true );
				} else if ( $item->post_type == 'ebook' ) {
					$ebooks[] = ebookModel::get( $item->ID, true );
				} else if ( $item->post_type == 'podcast' ) {
					$audiobooks[] = audiobook2Model::get( $item->ID, true );
				}
			}
		}

		return array_merge( $audiobooks, $ebooks );
	}

	public static function get_user_page( $user_id ) {
		global $wpdb;
		global $prefix;
		global $config;

		$user_object = $wpdb->get_row( "SELECT * FROM " . $prefix . "users WHERE " . $prefix . "users.ID = '$user_id'" );
		$usermetas   = get_user_meta( $user_id );
		$imgAvatar   = ( get_from_array( $usermetas, '_user_profile' ) ) ? get_from_array( $usermetas, '_user_profile' ) : $config['home_url'] . '/user-profile.jpg';
		if ( is_numeric( $imgAvatar ) ) {
			$imgAvatar = get_post_thumbnail_url( $imgAvatar );
		}
		$new_obj = array(
			'id'          => intval( $user_id ),
			'fullName'    => $user_object->display_name,
			'imageAvatar' => $imgAvatar,
			'description' => get_from_array( $usermetas, 'description' ),
			'items'       => userModel::get_author_contents( intval( $user_id ) )
		);

		return $new_obj;

	}

	public static function like_content( $user_id, $content_id ) {
		global $wpdb;
		global $prefix;

		if ( ! $content_id ) {
			return null;
		}

		$count = $wpdb->get_row( "SELECT * FROM " . $prefix . "postmeta WHERE post_id = $content_id AND meta_key = '_post_like_count'" );
		if ( $count ) {
			$likes_count = $count->meta_value;
			$check       = $wpdb->get_row( "SELECT * FROM $prefix" . "posts_feedback WHERE `user_id` = $user_id AND `content_id` = $content_id" );
			if ( $check ) {
				$wpdb->delete( $prefix . 'posts_feedback', array(
					'id' => $check->id
				) );
				$likes_count = intval( $likes_count ) - 1;
				$wpdb->update( $prefix . 'postmeta', array(
					'meta_value' => $likes_count
				), array(
					'post_id'  => $content_id,
					'meta_key' => '_post_like_count'
				) );
			} else {
				$likes_count = intval( $likes_count ) + 1;
				$wpdb->insert( $prefix . 'posts_feedback', array(
					'user_id'     => $user_id,
					'type'        => 1,
					'create_date' => date( 'Y-m-d H:i:s' ),
					'content_id'  => $content_id
				) );

				$wpdb->update( $prefix . 'postmeta', array(
					'meta_value' => $likes_count
				), array(
					'post_id'  => $content_id,
					'meta_key' => '_post_like_count'
				) );
			}
		} else {
			$wpdb->insert( $prefix . "postmeta", array(
				'meta_key'   => '_post_like_count',
				'meta_value' => 1,
				'post_id'    => intval( $content_id )
			) );

			$wpdb->insert( $prefix . 'posts_feedback', array(
				'user_id'     => $user_id,
				'type'        => 1,
				'create_date' => date( 'Y-m-d H:i:s' ),
				'content_id'  => $content_id
			) );
			$likes_count = 1;
		}

		return $likes_count;
	}
}
