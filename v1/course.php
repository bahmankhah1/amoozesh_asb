<?php

class Course{
    public function single($id, $short= false) {
		allowed_request_method( 'get' );
		$home = CourseModel::get($id, $short);
		api::send_result( 200, null, $home );
	}

	public function completeCourse($id) {
		allowed_request_method( 'get' );
		$course = CourseModel::getCompleteCourse($id);
		api::send_result( 200, null, $course );
	}

    public function catalog(){
		allowed_request_method( 'get' );
        $home = CourseModel::getList();
		api::send_result( 200, null, $home );
	}

	public function myCourses(){
		allowed_request_method( 'get' );
		$user_id = api::userFireWall();
        $home = CourseModel::myCourses($user_id);
		api::send_result( 200, null, $home );
	}
}