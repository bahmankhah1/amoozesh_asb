<?php

class Product{
    public function single($id, $short = false) {
		allowed_request_method( 'get' );
		$result = ProductModel::get($id, intval($short));
		api::send_result( 200, null, $result );
	}

    public function catalog($page_index = 1, $page_size = 10){
		allowed_request_method( 'get' );
        $result = ProductModel::getList($page_index, $page_size);
		api::send_result( 200, null, $result );
	}
}