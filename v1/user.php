<?php

class user
{
	public function requestCode()
	{
		api::allowed_request_method('post');
		global $db;
		global $json;
		if (filter_var($json->phone, FILTER_VALIDATE_EMAIL)) {
			// email


		} else {

			// phone number
			$phone = $json->phone;
			if ($phone) {

				$res['status'] = smsModel::send_login_code($phone);
				api::send_result(200, null, $res);
			} else {
				api::send_result('500', 'ارسال پیامک با مشکل مواجه شد', null);
			}
		}
	}

	public function requestCodeWeb()
	{
		api::allowed_request_method('post');
		$phone = $_POST['phone'];
		if ($phone) {
			if (smsModel::send_login_code($phone)) {
				$res['status'] = true;
			} else {
				$res['status'] = false;
			}
			api::send_result(200, null, $res);
		} else {
			api::send_result('500', 'ارسال پیامک با مشکل مواجه شد', null);
		}
	}

	public function updateFcm()
	{
		allowed_request_method('post');
		api::userFireWall();
		$r = UserModel::updateFcmToken();
		if ($r)
			api::send_result('200', 'Firebase token saved Successfully', null);
		else
			api::send_result('500', 'There was a problem saving firebase token', null);
	}
	public function checkLogin()
	{
		allowed_request_method('get');
		api::userFireWall();
		
		api::send_result('200', '', null);
		
	}


	public function validateCode()
	{
		allowed_request_method('post');
		global $json;
		$response = userModel::loginUser(intval($json->phone));

		if ($response) {
			api::send_result('200', null, array(
				'token'         => get_from_array($response, 'token'),
				'user'          => get_from_array($response, 'user_object'),
				'type'          => get_from_array($response, 'type'),
				'tokenValidate' => true
			));
		} else {
			api::send_result('403', 'کد وارد شده اشتباه است', null);
		}
	}

	public function updateUserName()
	{
		allowed_request_method('post');
		global $user_token;
		$user_id = userModel::validate();
		if (!$user_id) {
			api::send_result(403, 'forbidden', null);
		}

		global $wpdb;
		global $prefix;
		global $json;
		$result['status'] = $wpdb->update(
			$prefix . "users",
			array(
				'display_name' => $json->username
			),
			array(
				'ID' => $user_id
			)
		);
		$temp             = $wpdb->update(
			$prefix . "usermeta",
			array(
				'meta_value' => $json->username,
			),
			array(
				'user_id'  => $user_id,
				'meta_key' => 'last_name'
			)
		);

		if (!$temp) {
			$wpdb->insert($prefix . 'usermeta', array(
				'meta_key'   => 'first_name',
				'meta_value' => $json->username,
				'user_id'    => $user_id
			));
		}

		api::send_result(200, null, $result);
	}

	public function logout()
	{
		allowed_request_method('post');
		if (!userModel::validate()) {
			api::send_result(403, 'forbidden', null);
		}

		$result['status'] = userModel::logoutUser();
		api::send_result(200, null, $result);
	}

	public function addComment()
	{
		allowed_request_method('post');
		$user_id = api::userFireWall();

		global $json;

		$result['status'] = commentModel::add($user_id, $json->contentId, $json->text, $json->starsCount, $json->commentParent);
		api::send_result(200, null, $result);
	}

	public function profile()
	{
		allowed_request_method('post');
		$user_id = api::userFireWall();
		api::send_result(200, null, userModel::profile_object($user_id));
	}

	public function profilePhoto()
	{
		allowed_request_method('post');
		$user_id    = api::userFireWall();
		$url['url'] = userModel::upload_profile_picture($_FILES['file'], $user_id);
		api::send_result(200, null, $url);
	}

	public function authorPage()
	{
		allowed_request_method('post');
		global $json;
		if (isset($json->authorId)) {
			api::send_result(200, null, userModel::get_user_page($json->authorId));
		} else {
			api::send_result(404, 'authorId not found', null);
		}
	}

	public function likeContent()
	{
		allowed_request_method('post');
		$user_id = api::userFireWall();
		global $json;
		$content_id       = intval($json->contentId);
		$output['status'] = userModel::like_content($user_id, $content_id);
		api::send_result(200, null, $output);
	}
}
