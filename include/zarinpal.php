<?php

class zarinpal {
	public function paymentRequest( $amount, $description, $mobile, $order_id ) {
		global $config;
		global $prefix;
		global $wpdb;

		$data = array(
			"merchant_id"  => $config['zarinpal']['merchant'],
			"amount"       => $amount,
			"callback_url" => $config['home_url'] . "/pay/verify/$order_id",
			"description"  => $description,
			"metadata"     => [ "email" => "support@ariyanweb.com", "mobile" => $mobile ],
		);

		$jsonData = json_encode( $data );
		$ch       = curl_init( 'https://api.zarinpal.com/pg/v4/payment/request.json' );
		curl_setopt( $ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1' );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen( $jsonData )
		) );

		$result = curl_exec( $ch );
		$err    = curl_error( $ch );
		$result = json_decode( $result, true, JSON_PRETTY_PRINT );
		curl_close( $ch );

		if ( $err ) {
			echo "cURL Error #:" . $err;
		} else {
			if ( empty( $result['errors'] ) ) {
				if ( $result['data']['code'] == 100 ) {
					$res = $wpdb->update( $prefix . 'user_orders',
						array(
							'auth_key'    => $result['data']["authority"],
							'create_date' => date( 'Y-m-d H:i:s' ),
						),
						array(
							'id' => $order_id,
						) );
					if ( $res ) {
						header( 'Location: https://zarinpal.com/pg/StartPay/' . $result['data']["authority"] );
					}
				}
			} else {
				echo 'Error Code: ' . $result['errors']['code'];
				echo 'message: ' . $result['errors']['message'];

			}
		}
	}

	public function paymentVerify( $auth_key ) {
		global $wpdb;
		global $prefix;
		global $config;

		$app_payment = $wpdb->get_row( "SELECT * FROM `$prefix" . "user_orders` WHERE `auth_key` LIKE '$auth_key'" );
		$order_id    = $app_payment->order_id;
		$amount      = intval( $app_payment->price );

		$data     = array(
			"merchant_id" => $config['zarinpal']['merchant'],
			"authority"   => $auth_key,
			"amount"      => $amount
		);
		$jsonData = json_encode( $data );
		$ch       = curl_init( 'https://api.zarinpal.com/pg/v4/payment/verify.json' );
		curl_setopt( $ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v4' );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen( $jsonData )
		) );

		$result = curl_exec( $ch );
		curl_close( $ch );
		$result = json_decode( $result, true );

		if ( isset( $result['data']['code'] ) && ( $result['data']['code'] == '100' || $result['data']['code'] == '101' ) ) {
			$wpdb->update( $prefix . 'user_orders', array(
				'status' => 1,
				'ref_id' => $result['data']['ref_id']
			), array(
				'auth_key' => $auth_key
			) );

			global $order_id;
			global $ref_id;
			$ref_id = $result['data']['ref_id'];
			require_once 'success.php';
			exit;
		} else {
			global $order_id;
			require_once 'error.php';
			exit;
		}

	}
}